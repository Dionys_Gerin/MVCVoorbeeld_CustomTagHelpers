﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LesVoorbeeldCustomHelpersAttributes.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldCustomHelpersAttributes.Controllers
{
 
    public class StudentController : Controller
    {
        public IActionResult Index()
        {
            Student student = new Student() { Id = 300, Naam = "Johnny", Afstudeergraad = Graad.Voldoende };
            return View(student);
        }

    }

}
